#!/bin/bash

#
# General settings
#

ARCH="x86_64"
DISTRO="ipfire3"
VERSION="$(date +"%Y%m%d")"
WORKING_DIR=$(mktemp -d /var/tmp/ipfire3_image.XXXXXXXX)

#
# Image
#

IMAGE_BASE_FILE="$(mktemp -u ${WORKING_DIR}/image_base_file.XXXXXXX.img)"
IMAGE_SIZE="7500"
IMAGE_MOUNT_DIR="$(mktemp -d ${WORKING_DIR}/image.XXXXXXX)"

IMAGE_TYPES_PUBLISH="qcow2 vmdk vdi img"
IMAGE_DIR_PUBLISH="/home/jschlag/public/ipfire3-images"

# The used filesystem.
FILESYSTEM="xfs"

# Additional packages which should be installed.
PACKAGES="xfsprogs kernel openssh-server wget htop tmux"

# Use git network stack. ( When using the git network stack,
# development tools and git automatically will be installed.)
USE_GIT_NETWORK_STACK="True"

# List of packages which are required to build the network stack.
NETWORK_BUILD_DEPS="asciidoc autoconf automake docbook-xsl libnl3-devel libxslt systemd-devel"

# Install development tools.
INSTALL_DEV_PACKAGES="True"

# List of development tools which should be installed.
DEVELOPMENT_PACKAGES="make gcc libtool git"


# Git repositories which also should be checked, out.
GIT_REPOS=""

#
# Stuff for the local repo
#

# Use local repository.
USE_LOCAL_REPO="True"

# Source path for the local repo packages.
LOCAL_REPO_SOURCE_PATH="/var/lib/pakfire/local/"

# Path were the local repo is created
LOCAL_REPO_DIR="$(mktemp -d ${WORKING_DIR}/ipfire-repo.XXXXXXX)"

# Config file for the local repo
LOCAL_REPO_FILE="/etc/pakfire/repos/local.repo"
