#!/bin/bash
###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

#Path of the script

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"


# INCLUDE SETTINGS
. "${SCRIPT_PATH}/settings.sh"

# INCLUDE FUNCTIONS
. "${SCRIPT_PATH}/functions.sh"
#
# Scripts starts here
#

log DEBUG "Working dir is ${WORKING_DIR}"


#Parse cmdline
parse_cmdline $@

# Check that pakfire is working
check_for_pakfire

if ! check_for_free_space 10000 "${WORKING_DIR}"; then
	exit ${EXIT_ERROR}
fi

# Check that the image does not exist yet
if [ -f ${IMAGE_BASE_FILE} ]; then
	log ERROR "Image file does already exists"
	exit 1
fi

# Check that the local repo file does not exists yet.
# We do not want to override custom user configurations.
if [ -f "${LOCAL_REPO_FILE}" ]; then
	log ERROR "Config file ${LOCAL_REPO_FILE} for the local repo does already exists"
	exit 1
fi

# cd into working directory
cd ${WORKING_DIR} || exit ${EXIT_ERROR}

#
## Create the disk image.
#
dd if=/dev/zero of=${IMAGE_BASE_FILE} seek=${IMAGE_SIZE}M count=1k bs=1

# Setup the loopback device.
outlo=`losetup -f --show ${IMAGE_BASE_FILE}`

log INFO "Loop device is ${outlo}"
log INFO "Create partions and filesystem"

# Create and msdos compatible table on the image.
parted ${outlo} mklabel msdos

# Add a new partition to the image.
parted ${outlo} mkpart primary ${FILESYSTEM} 2048k 100% -a minimal

# Make the primary partition bootable.
parted ${outlo} set 1 boot on

# Notify the kernel about the new partition.
partx -a ${outlo}

#
## Create the filesystem.
#
mkfs.${FILESYSTEM} ${outlo}p1

#
## Mount the filesystem.
#

log INFO "Mount partion in ${IMAGE_MOUNT_DIR}"

# Afterwards mount the image.
mount -t ${FILESYSTEM} ${outlo}p1 ${IMAGE_MOUNT_DIR}

#
## Install IPFire 3.x.
#

# Add grub on x86_64 to the package list.
if [ "${ARCH}" == "x86_64" ] || [ "${ARCH}" == "i686" ]; then
	PACKAGES="${PACKAGES} grub"

	# Store, that grub is present.
	HAVE_GRUB="True"
fi

# Check if the git network stack should be installed.
if [ "${USE_GIT_NETWORK_STACK}" == "True" ]; then
	GIT_REPOS="${GIT_REPOS} git://git.ipfire.org/network.git"

	# Add build dependencies of network package.
	PACKAGES="${PACKAGES} ${NETWORK_BUILD_DEPS}"
fi

# Add develoment packes to the package list, if required.
if [ "${INSTALL_DEV_PACKAGES}" == "True" ] || [ ! -z "${GIT_REPOS}" ]; then
	PACKAGES="${PACKAGES} ${DEVELOPMENT_PACKAGES}"
fi

log INFO "Create local respository"


# Check if the local repo should be used.
if [ "${USE_LOCAL_REPO}" == "True" ]; then
	# Create local repository.
	mkdir -pv "${LOCAL_REPO_DIR}"

	# Master repository.
 	if ! pakfire-server repo create ${LOCAL_REPO_DIR} ${LOCAL_REPO_SOURCE_PATH}; then
		log ERROR "Failed to create a local respository"
		cleanup
		exit 1
	fi

	# Create temporary pakfire repo file.
	echo "[repo:local]" >> "${LOCAL_REPO_FILE}"
	echo "description = Local repository." >>  "${LOCAL_REPO_FILE}"
	echo "enabled = 0" >>  "${LOCAL_REPO_FILE}"
	echo "baseurl = ${LOCAL_REPO_DIR}" >>  "${LOCAL_REPO_FILE}"

	ENABLE_LOCAL="--enable-repo=local"
fi

# Install IPFire 3.x in the created image.
yes | pakfire --root=${IMAGE_MOUNT_DIR} ${ENABLE_LOCAL} install @Base ${PACKAGES}

#
# Enable serial console
#


#echo "GRUB_TERMINAL=\"serial console\"" >> "${IMAGE_MOUNT_DIR}/etc/default/grub"
#echo "GRUB_SERIAL_COMMAND=\"serial --unit=0 --speed=115200\"" >> "${IMAGE_MOUNT_DIR}/etc/default/grub"

#Hack to install a /etc/default/grub file

cmd cp -f "${SCRIPT_PATH}/grub" "${IMAGE_MOUNT_DIR}/etc/default"

#
## Generate fstab
#

# Gather the uuid of the partition.
FS_UUID=$(blkid -o value -s UUID ${outlo}p1)

# Write fstab.
echo "UUID=${FS_UUID} / ${FILESYSTEM} defaults 0 0" > "${IMAGE_MOUNT_DIR}/etc/fstab"

cat  "${IMAGE_MOUNT_DIR}/etc/fstab"

#
## Remove the password for user root.
#

reset_root_password "${IMAGE_MOUNT_DIR}"

#
## Setup git repositories.
#

clone_git_repos "${IMAGE_MOUNT_DIR}/build" ${GIT_REPOS}


#
## Prepare chrooting into the image.
#

chroot_script_init

if [ "${HAVE_GRUB}" == "True" ]; then
	chroot_script_add_cmd "grub-install --boot-directory=/boot/ ${outlo}" "grub-mkconfig -o /boot/grub/grub.cfg"
fi



# ENABLE Serial Console
chroot_script_add_cmd "/bin/systemctl enable getty@.service"


# Check if the network stack should be build.
if [ "${USE_GIT_NETWORK_STACK}" == "True" ]; then
	chroot_script_add_cmd "cd /build/network" "./autogen.sh" "./configure" "make" "make install"
fi

chroot_script_exec


# Insert the UUID because grub-mkconfig often fails to
# detect that correctly

sed -i "${IMAGE_MOUNT_DIR}/boot/grub/grub.cfg" \
		-e "s/root=[A-Za-z0-9\/=-]*/root=UUID=${FS_UUID}/g"

cat "${IMAGE_MOUNT_DIR}/boot/grub/grub.cfg"

cat  "${IMAGE_MOUNT_DIR}/etc/fstab"


#
## Tidy up.
#

# Wait a second.
sleep 5

# Check filesystem for damage.
fsck.${FILESYSTEM} ${outlo}p1

cleanup_stage_1

publish "${IMAGE_DIR_PUBLISH}" "${IMAGE_BASE_FILE}"

# Cleanup
cleanup_stage_2
