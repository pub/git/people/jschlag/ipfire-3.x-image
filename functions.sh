#!/bin/bash

# Constants

# Proper error codes
EXIT_OK=0
EXIT_ERROR=1
EXIT_CONF_ERROR=2
EXIT_NOT_SUPPORTED=3
EXIT_NOT_HANDLED=4
EXIT_COMMAND_NOT_FOUND=127
EXIT_ERROR_ASSERT=128

EXIT_TRUE=0
EXIT_FALSE=1
EXIT_UNKNOWN=2

TRUE=0
FALSE=1

IMAGE_RELEASE=${FALSE}

CHROOT_SCRIPT="/build/chroot.sh"


# Functions

log() {
	local level=${1}
	local message=${2}
	echo "[${level}] ${message}"
}

cmd() {
	local cmd="$@"
	local ret

	log DEBUG "Running command: ${cmd}"

	${cmd}
	ret=$?

	case "${ret}" in
		${EXIT_OK})
			return ${EXIT_OK}
			;;
		*)
			log DEBUG "Returned with code '${ret}'"
			return ${ret}
			;;
	esac
}


is_mounted() {
	local mounted_dir=${1}

	if [ ! -d ${mounted_dir} ]; then
		log ERROR "Is not a directory ${mounted_dir}"
		return ${FALSE}
	else
		mountpoint ${mounted_dir}
	fi
}

unmount_dirs() {
	local mounted_dir
	local path

	local return_value=${EXIT_OK}

	for mounted_dir in "/proc" "/sys" "/dev/pts" "/dev/shm" "/dev" "/run" "/tmp" ""
	do
		path="${IMAGE_MOUNT_DIR}${mounted_dir}"

		if is_mounted "${path}"; then
			log DEBUG "Unmounting ${path}"
			umount  "${path}"
			# Check once again
			if is_mounted "${path}"; then
				return_value=${EXIT_ERROR}
			fi
		else
			log DEBUG "${path} is not  mounted"
		fi
	done

}




cleanup_stage_1() {

	if unmount_dirs; then

		# Remove partition from the kernel table.
		partx -d ${outlo}p1

		# Remove loopback device
		log DEBUG " Remove loopback device"
		losetup -d ${outlo}
	fi


	# Cleanup Config file for the local repo
	if [ -f "${LOCAL_REPO_FILE}" ]; then
		rm -f "${LOCAL_REPO_FILE}"
	fi

}


cleanup_stage_2() {
	if unmount_dirs; then
		# Drop working dir
		if [ -d "${WORKING_DIR}" ]; then
			#rm -dfR "${WORKING_DIR}"
			echo ""
		fi
	fi
}

cleanup() {
	cleanup_stage_1
	cleanup_stage_2
}

generate_image_filename() {
	local distro=${1}
	local version=${2}
	local arch=${3}

	echo "${distro}-${version}-${arch}"
}

check_for_pakfire() {
	local return_value=0

	# TODO
	# Check that pakfire-server binary is available
	# Check that pakfire binary is available

	# Check that repo files are installed. (pakfire need to know which repos exist)
	local repo_dir="/etc/pakfire/repos"

	if [ ! -d "${repo_dir}" ]; then
		log ERROR "Could not find respository directory ${repo_dir}"
		return_value=1
	fi

	return ${return_value}
}

compress_image() {
	local compression=${1}
	local image_file=${2}
	local level=${3}

	log debug "Compressing ${image_file} with ${compression}"

	case "${compression}" in
		"xz")
			# Check that the file does not exist yet
			if [ -f "${image_file}.xz" ]; then
				log ERROR "Failed to compress the image. The file already exists"
				return ${EXIT_ERROR}
			fi
			cmd xz "-${level}" "${image_file}"
			;;
		"zip")
		# Check that the file does not exist yet
			if [ -f "${image_file}.zip" ]; then
				log ERROR "Failed to compress the image. The file already exists"
				return ${EXIT_ERROR}

			fi
			cmd zip "-${level}" "${image_file}.zip" "${image_file}"
			# Remove the file which we compressed+
			rm -f "${image_file}"
			;;

	esac
}

reset_root_password() {
	local root_dir=${1}
	# Backup passwd file
	cp -avf ${root_dir}/etc/passwd ${root_dir}/etc/passwd.orig

	# Drop root password.
	sed -e "s/^\(root:\)[^:]*:/\1:/" ${root_dir}/etc/passwd.orig > ${root_dir}/etc/passwd

	# Remove passwd backup file.
	rm -rvf ${root_dir}/etc/passwd.orig
}

clone_git_repos() {
	# Dir where the repos should be located
	local dir=${1}
	shift
	local repos="$@"

	local repo

	mkdir -pv ${dir}

	(
		cd ${dir}
		# Clone git repositories.
		for repo in ${repos}; do
			git clone ${repo}
		done
	)
}

# This function is used to publish the produced images

publish() {
	local path=${1}
	# The image we created usually a img. file
	local image_base_file=${2}

	local image_name_final="$(generate_image_filename "${DISTRO}" "${VERSION}" "${ARCH}")"
	local image_type

	# Do these steps for every image format we like to publish
	for image_type in ${IMAGE_TYPES_PUBLISH}; do
		# Convert images to the type specified in IMAGE_TYPES_PUBLISH
		convert_image "${image_type}" "${image_base_file}" "${image_name_final}"

		# compress image.
		if [[ ${IMAGE_RELEASE} -eq ${TRUE} ]]; then
			local compression_type
			local compression_level


			# Get compressioon type
			compression_type="$(get_compression_type ${image_type})"

			compression_level=1
			compress_image "${compression_type}" "${image_name_final}.${image_type}" ${compression_level}

			# Move images to this path
			mv -f "${image_name_final}.${image_type}.${compression_type}" ${path}

			# point the latest links to these images
			ln -s -f "${path}/${image_name_final}.${image_type}.${compression_type}" \
				"${path}/$(generate_image_filename "${DISTRO}" "latest" "${ARCH}").${image_type}.${compression_type}"
		else

			# Move images to this path
			mv -f "${image_name_final}.${image_type}" ${path}
		fi
	done

}

convert_image() {
	local type=${1}
	local from=${2}
	local to=${3}

	if [[ ${type} = "img" ]]; then
		# We do not need to convert the image here but we need to rename
		mv -f ${from} ${to}.${type}
		return $?
	fi

	if [[ ${type} = "qcow2" ]]; then
		local command="qemu-img convert -c -O ${type} ${from} ${to}.${type}"
	else
		local command="qemu-img convert -O ${type} ${from} ${to}.${type}"
	fi

	cmd ${command}
}

get_compression_type() {
	local image_type=${1}

	case "${image_type}" in
		"qcow2" | "img")
			# These types can be used only under Unix so we use xz as compression
			echo "xz"
			;;
		"vmdk" | "vdi")
			# These types can be also under Windows so we use zip as compression
			echo "zip"
			;;
	esac

}

check_for_free_space() {
		local space=${1}
		local path=${2}
		local space_in_path=0

		space_in_path=$(df -h -B MB --output=avail ${path} | tail -n 1)
		space_in_path=${space_in_path%MB}
		log debug ${space_in_path}
		log debug ${space}

		if [ ${space_in_path}  -lt ${space} ]; then
			log error "Not enough free space available under ${path}"
			log error "Free space is ${space_in_path}MB but we need at least ${space}MB"
			return ${EXIT_ERROR}
		fi
}

parse_cmdline() {
	while [ $# -gt 0 ]; do
		case "${1}" in
			"--release")
					IMAGE_RELEASE=${TRUE}
				;;

			*)
				error "Invalid argument: ${1}"
				return ${EXIT_CONF_ERROR}
				;;
		esac
		shift
	done
}

chroot_wrapper() {
	local chroot_dir="${1}"

	shift

	local command

	if [ ! -d ${chroot_dir} ]; then
		log ERROR "Cannot chroot in a non directory ${chroot_dir}"
	fi

	mount proc "${chroot_dir}/proc" -t proc -o nosuid,noexec,nodev
	mount sys "${chroot_dir}/sys" -t sysfs -o nosuid,noexec,nodev,ro
	mount udev "${chroot_dir}/dev" -t devtmpfs -o mode=0755,nosuid
	mount devpts "${chroot_dir}/dev/pts" -t devpts -o mode=0620,gid=5,nosuid,noexec
	mount shm "${chroot_dir}/dev/shm" -t tmpfs -o mode=1777,nosuid,nodev
	mount /run "${chroot_dir}/run" --bind
	mount tmp "${chroot_dir}/tmp" -t tmpfs -o mode=1777,strictatime,nodev,nosuid

	for command in "$@"
	do
		cmd chroot "${chroot_dir}" "${command}"
	done

	umount "${chroot_dir}/proc"
	umount "${chroot_dir}/sys"
	umount "${chroot_dir}/dev/pts"
	umount "${chroot_dir}/dev/shm"
	umount "${chroot_dir}/dev"
	umount "${chroot_dir}/run"
	umount "${chroot_dir}/tmp"
}

chroot_script_init(){
	echo "#!/bin/bash" > "${IMAGE_MOUNT_DIR}${CHROOT_SCRIPT}"
}

chroot_script_add_cmd(){
	local command
	for command in "$@"
	do
	echo "${command}" >>  "${IMAGE_MOUNT_DIR}${CHROOT_SCRIPT}"
	done
}


chroot_script_exec() {
	chmod +x  "${IMAGE_MOUNT_DIR}${CHROOT_SCRIPT}"

	chroot_wrapper ${IMAGE_MOUNT_DIR}  "${CHROOT_SCRIPT}"
}
